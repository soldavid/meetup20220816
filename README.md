# AWS CDK - Cloud Development Kit

![CDK Logo](images/cdk-logo.png)

## Grupo de Usuarios de AWS Caracas

2022-08-16

![AWS UG Caracas Logo](images/caracas.png)

<https://gitlab.com/soldavid/meetup20220816>

## AWS CDK Workshop

<https://cdkworkshop.com/>

## Requisitos

- [Instalar AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
- [Cuenta y Usuario de AWS](https://cdkworkshop.com/15-prerequisites/200-account.html)
- [Instalar Node.js](https://cdkworkshop.com/15-prerequisites/300-nodejs.html). Si están en Linux o MacOS les recomiendo con [nvm](https://github.com/nvm-sh/nvm)
  - O si vas a utilizar otro lenguaje ([Python](https://cdkworkshop.com/15-prerequisites/600-python.html), [.NET](https://cdkworkshop.com/15-prerequisites/700-dotnet.html), [Java](https://cdkworkshop.com/15-prerequisites/800-java.html)) proceder a configurarlo.
- [Acceso a un IDE (Visual Studio Code por ejemplo)](https://cdkworkshop.com/15-prerequisites/400-ide.html)
- [Instalar AWS CDK Toolkit](https://cdkworkshop.com/15-prerequisites/500-toolkit.html)

## [Infraestructura como Código](https://en.wikipedia.org/wiki/Infrastructure_as_code)

En vez de crear la infraestructura "en la consola" automatizar y documentar el despliege de recursos a través de código.

## [DevOps](https://en.wikipedia.org/wiki/DevOps)

Crear un flujo de trabajo integrado entre las actividades de desarrollo y producción, automatizando los procedimeinto, permitiendo el dar valor a los clientes de forma más rápida y segura.

## [Control de Versiones](https://en.wikipedia.org/wiki/Version_control)

Mantener el código, y las diversas versiones del mismo, en un repsotiorio que permita el control del mismo.

## [AWS CloudFormation](https://aws.amazon.com/cloudformation/)

Herramienta Declarativa de Infraestructura como Código para AWS.

## [AWS CDK (Cloud Development Kit)](https://aws.amazon.com/cdk/)

Framework para hacer Infraestructura como Código utilizando lenguajes de programación populares.

### Componentes

- AWS CDK CLI (Command Line Interface)
- AWS CDK Framework
- AWS Construct Library

![Product Diagram](images/product-diagram.png)

## Lenguajes

- JavaScript
- TypeScript
- Python
- Java
- .NET

## Conceptos CDK

- Aplicación
- Stack
- Construct
- CFN Resources

![Arquitectura CDK](images/appstacks.png)

## Comandos básicos

- `cdk bootstrap aws://<account>/<region>` o `cdk bootstrap --app <application>`  
  Inicializa CDK, crea una cubeta para los artefactos
- `cdk init`  
  Crea una nueva aplicación
- `cdk list`  
  Muestra los stacks en la aplicación
- `cdk synth > cloudformation_template.yaml`  
  Genera el script de CloudFormation en la consola
- `cdk diff`  
  Muestra los cambios a realizar
- `cdk deploy`  
  Despliega la aplicación en nuestra cuenta
- `cdk destroy`  
  Destruye todos los recursos

> Nota: Se puede agregar el parámetro `<stack>` para desplegar solo un stack de la aplicación.  
> Nota: Agregar `--profile <profile>` para definir el profile a utilizar

## Código en Typescript

```typescript
export class MyEcsConstructStack extends core.Stack {
  constructor(scope: core.App, id: string, props?: core.StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, "MyVpc", {
      maxAzs: 3, // Default is all AZs in region
    });

    const cluster = new ecs.Cluster(this, "MyCluster", {
      vpc: vpc,
    });

    // Create a load-balanced Fargate service and make it public
    new ecs_patterns.ApplicationLoadBalancedFargateService(
      this,
      "MyFargateService",
      {
        cluster: cluster, // Required
        cpu: 512, // Default is 256
        desiredCount: 6, // Default is 1
        taskImageOptions: {
          image: ecs.ContainerImage.fromRegistry("amazon/amazon-ecs-sample"),
        },
        memoryLimitMiB: 2048, // Default is 512
        publicLoadBalancer: true, // Default is false
      }
    );
  }
}
```

## Código en CloudFormation

[Ejemplo Fargate](ejemplo_fargate.yaml)

## Referencias

- [Workshop](https://cdkworkshop.com/)
- [AWS Cloud Development Kit (AWS CDK) Developer Guide](https://docs.aws.amazon.com/cdk/v2/guide/home.html)
- [Construct Hub](https://constructs.dev)
- [Repositorio GitHub](https://github.com/aws/aws-cdk)
- [AWS CDK API Reference](https://docs.aws.amazon.com/cdk/api/v2/docs/aws-construct-library.html)
- [AWS CDK Examples](https://github.com/aws-samples/aws-cdk-examples)
- [Awesome CDK](https://github.com/eladb/awesome-cdk)

## Muchas gracias
